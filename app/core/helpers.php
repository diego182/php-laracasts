<?php

/**
* dump variavel com <pre> e depois die
* @param mixed $var variável para ser apresentada
*/
function dd($var)
{
  echo "<pre>";
  var_dump($var);
  echo "</pre>";

  die();
}

function view($view, $data = [])
{
  extract($data);

  return require 'views/'.$view.'.view.php';
}

function redirect($path)
{
  return header("Location: /{$path}");
}
