<?php


namespace App\Core;

class Router
{
  public $routes = [
    'GET' => [],
    'POST' => [],
    'PUT' => [],
    'PATCH' => [],
    'DELETE' => []
  ];

  public static function load($file)
  {
    $router = new static; // instancio router estático

    require $file; // carrego as rotas

    return $router; // retorno objeto
  }

  public function get($uri, $controller)
  {
    $this->routes['GET'][$uri] = $controller;
  }

  public function post($uri, $controller)
  {
    $this->routes['POST'][$uri] = $controller;
  }

  public function put($uri, $controller)
  {
    $this->routes['PUT'][$uri] = $controller;
  }

  public function patch($uri, $controller)
  {
    $this->routes['PATCH'][$uri] = $controller;
  }

  public function delete($uri, $controller)
  {
    $this->routes['DELETE'][$uri] = $controller;
  }

  public function direct($uri, $method)
  {
    if (array_key_exists($uri, $this->routes[$method])) {

      $request = null;
      if($method == 'POST') {
        $request = new Request();

        foreach ($_POST as $key => $value) {
          $request->$key = $value;
        }
      }
      // Se a URI existir para o dado Método, vou chamar a ação relacionada
      // do controller específico
      return $this->callAction(
        $request,
        ...explode('@', $this->routes[$method][$uri])
      );

    }
    else {
      throw new Exception("Nenhuma rota definida para {$uri}");
    }
  }

  protected function callAction($request, $controller, $action)
  {
    $controller = 'App\\Controllers\\'.$controller;
    $controller = new $controller;

    if(!method_exists($controller, $action)) {
      throw new Exception("{$controller} does not respond to {$action}");
    }


    if($request)
      return $controller->$action($request);

    return $controller->$action();

  }
}
