<?php

class QueryBuilder
{
  protected $pdo;
  protected $class;

  public function __construct($pdo)
  {
    $this->pdo = $pdo;
  }

  public function selectAll($table, $class = null)
  {
    $statement = $this->pdo->prepare("select * from {$table}");
    $statement->execute();

    if($class)
      return $statement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $class);

    return $statement->fetchAll(PDO::FETCH_OBJ);
  }

  public function insert($table, $data)
  {

    // Gerar a query string
    $params = implode(', ', array_keys($data));
    $param_names = ':'.implode(', :', array_keys($data));
    $query_str = "insert into {$table} ($params) values ($param_names)";

    $statement = $this->pdo->prepare($query_str);

    $statement->execute($data);
  }

}
