<?php require 'partials/head.php'; ?>

<h2>Add a Task</h2>

<form action="/tasks" method="post">
  <input name="task" type="text">
  <button type="submit">Submit</button>
</form>

<ul>
  <?php foreach ($tasks as $task) : ?>
    <li>
      <?= $task->isComplete() ? '<strike>'.$task->getDescription().'</strike>' : $task->getDescription() ?>
    </li>
  <?php endforeach ?>
</ul>


<?php require 'partials/footer.php'; ?>
