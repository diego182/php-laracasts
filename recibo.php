<?php

require 'functions.php';
require 'custom/Recibo.php';



// // instantiate and use the dompdf class
// $dompdf = new Dompdf();
//
// $html = file_get_contents("recibo2.pdf.html");
//
$company = "Excellence Soft";
$value = floatval($_GET['valor']);
$reference = "Honorário contábil - Baixa no verso";
$date = "Fortaleza, 20 de Março de 2017";
$emitter = "Edvan Farias";
$crc = "CRC: 9409/0-3";

$vias = intval($_GET['vias']);

Recibo::generate($vias,$value, $company, $reference, $date);
//
// $html = str_replace('{{$company}}', mb_strtoupper($company), $html);
// $html = str_replace('{{$value}}', $value, $html);
// $html = str_replace('{{$value_text}}', $value_text, $html);
// $html = str_replace('{{$reference}}', mb_strtoupper($reference), $html);
// $html = str_replace('{{$date}}', $date, $html);
// $html = str_replace('{{$emitter}}', $emitter, $html);
// $html = str_replace('{{$crc}}', $crc, $html);
//
// $dompdf->loadHtml($html);
//
// // (Optional) Setup the paper size and orientation
// $dompdf->setPaper('A4', 'portrait');
//
// // Render the HTML as PDF
// $dompdf->render();
//
// // Output the generated PDF (1 = download and 0 = preview)
// $dompdf->stream("recibo-20032017",array("Attachment"=>0));
